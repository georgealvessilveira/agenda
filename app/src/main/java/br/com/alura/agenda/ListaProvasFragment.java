package br.com.alura.agenda;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Arrays;
import java.util.List;

import br.com.alura.agenda.modelo.Prova;

public class ListaProvasFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lista_provas,
                container, false);

        List<String> topicosPort = Arrays.asList("Sujeito", "Objeto direto", "Objeto indireto");
        Prova provaPort = new Prova("Português", "25/05/2016", topicosPort);

        List<String> topicosMat = Arrays.asList("Equações de 2º grau", "Trigonometria");
        Prova provaMat = new Prova("Matemática", "25/05/2016", topicosMat);

        List<Prova> provas = Arrays.asList(provaPort, provaMat);
        ArrayAdapter<Prova> adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1, provas);

        ListView lista = view.findViewById(R.id.provas_lista);
        lista.setAdapter(adapter);

        lista.setOnItemClickListener((parent, v, position, id) -> {
            Prova prova = (Prova) parent.getItemAtPosition(position);

            ProvasActivity provasActiviy = (ProvasActivity) getActivity();
            provasActiviy.selecionaProva(prova);
        });

        return view;
    }
}
